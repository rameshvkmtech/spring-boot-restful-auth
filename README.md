# README #

Securing Restful APIs With JWTs
JSON Web Tokens, commonly known as JWTs, are tokens that are used to authenticate users on applications. This technology has gained popularity over the past few years because it enables backends to accept requests simply by validating the contents of these JWTs. That is, applications that use JWTS no longer have to hold cookies or other session data about their users. This characteristic facilitates scalability while keeping applications secure.

During the authentication process, when a user successfully logs in using their credentials, a JSON Web Token is returned and must be saved locally (typically in local storage). Whenever the user wants to access a protected route or resource (an endpoint), the user agent must send the JWT, usually in the Authorizationheader using the Bearer schema, along with the request.

When a backend server receives a request with a JWT, the first thing to do is to validate the token. This consists of a series of steps, and if any of these fails then the request must be rejected. The following list shows the validation steps needed:

Check that the JWT is well formed.
Check the signature.
Validate the standard claims.
Check the Client permissions (scopes)..

### What is this repository for? ###

The Restful Spring Boot API Overview
The RESTful Spring Boot API that we are going to secure is a task list manager. The task list is kept globally, which means that all users will see and interact with the same list

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Conclusion ###
Securing RESTful Spring Boot API with JWTs is not a hard task. This article showed that by creating a couple of classes and extending a few others provided by Spring Security, we can protect our endpoints from unknown users, enable users to register themselves and authenticate existing users based on JWTs.

Of course, for it to be a production-ready application we would need a few more features, like password retrieval, but, hopefully, this article demystified the most sensible parts of dealing with JWTs to authorize requests on Spring Boot applications